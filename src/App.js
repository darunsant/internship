import React, { Component, Fragment } from 'react'
import { Card, Button, CardBody, CardText, Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import styles from './style.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: {},
      error:''
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);

  }
  handleEmailChange(e) {
    console.log(e.target.value)
    this.setState({ email: e.target.value });
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  }
  
  onUserLogin() {
    console.log(this.state.email)
    console.log(this.state.password)
    axios.post(`http://localhost:3000/api/login`,{
      email:this.state.email,
      password : this.state.password
    })
    .then(res=>{
      console.log(res.status)
      if(res.status == 200){
        alert('Login Successed');
      }
      else if(res.status == 401){
        this.setState({error:"Error"})  
      }
    })
    this.setState({email:"",password:""});

  }

  render() {
    return (
      <Fragment >
        <div style ={{backgroundColor:"ghostwhite"}}>
      <Row style = {{padding:"20vh 0"}}>
        
      <Col sm="12" md={{ size: 5, offset: 3 }} className="mx-auto my-auto" >
        <Card>
          <CardBody>
              <div class="d-flex justify-content-center align-items-center">
                  <img src ="https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg" style ={{width:180}}/>
                </div>
              <Form>
                <FormGroup>
                  <Label for="exampleEmail">Email</Label>
                    <Input type="email"
                          defaultValue={this.state.email}
                          name={'email'}
                          id = "email"
                          onChange={this.handleEmailChange}
                        />                </FormGroup>
                <FormGroup>
                  <Label for="examplePassword">Password</Label>
                      <Input type="password"
                          defaultValue={this.state.password}
                          name={'password'}
                           id = "password"
                          onChange={this.handlePasswordChange}
                        />               
                         </FormGroup>
                <div style = {{color:"red"}}>{this.state.errors["status"]}</div>
      {this.state.error}
                <div class="d-flex justify-content-center">
                  <Button
                    onClick={() => this.onUserLogin()}
                  >SIGN IN</Button>
                </div>
                
              </Form>
              <div className="d-flex justify-content-between align-items-center" style ={{color:"deepskyblue"}}>
                  <a href="#" >Forget Password?</a> 
                  <a href="#">Create a new account</a>          
              </div>
          </CardBody>
        </Card>
      </Col>
    </Row>
    </div>
    </Fragment>
    )
  }
}

export default App
